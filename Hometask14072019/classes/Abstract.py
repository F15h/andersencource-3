from abc import ABC, abstractmethod
import math


class Figures(ABC):
    def info(self):
        print('Фигура:')


    @abstractmethod
    def square(self):
        pass

    @abstractmethod
    def perimeter(self):
        pass


class Rectangle(Figures):
    def __init__(self, side1, side2):
        self.side1 = side1
        self.side2 = side2


    def info(self):
        super().info()
        print('Прямоугольник со сторонами', self.side1, 'и', self.side2, ';', 'площадью', self.square(), ',', 'периметром', self.perimeter())


    def square(self):
        return self.side1 * self.side2


    def perimeter(self):
        return 2 * self.side1 + 2 * self.side2


class Circle(Figures):
    def __init__(self, rad):
        self.rad = rad


    def info(self):
        super().info()
        print('Круг с радиусом', self.rad, ';', 'площадью', self.square(), ',', 'периметром', self.perimeter())


    def square(self):
        return math.pi * self.rad**2


    def perimeter(self):
        return 2 * math.pi * self.rad


class Triangle(Figures):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        #self.p = 0.5 * (self.a +self.b + self.c)
        #self.h = 2 * math.sqrt(self.p * (self.p - self.a) * (self.p - self.b) * (self.p - self.c)) / self.a


    def servh(self):
        p = 0.5 * (self.a + self.b + self.c)
        h = 2 * math.sqrt(p * (p - self.a) * (p - self.b) * (p - self.c)) / self.a
        return h



    def info(self):
        super().info()
        print('Треугольник со сторонами', self.a, ',', self.b, ',', self.c, ';', 'площадью', self.square(), ',', 'периметром', self.perimeter())


    def square(self):
        high = self.servh()
        return 0.5 * self.a * high


    def perimeter(self):
        return self.a +self.b + self.c