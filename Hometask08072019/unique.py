f = str(input('Введите список чисел: '))


list1 = [int(f) for f in f.split(' ')]


list2 = [i for i in list1 if list1.count(i) >= 2]


unique = set(list2)


print(list1)
print(list2)
print(unique)