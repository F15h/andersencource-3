def gen(n):
    a, b = int(input('Введите первое число: ')), int(input('Введите второе число: '))
    for i in range(n):
        yield a
        a, b = b, a + b
print(*gen(10))
