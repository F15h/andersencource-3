# -*- coding: utf-8 -*-
import json
from filtercount import filtercount, filteryear


with open('data_file.json', "r", encoding="utf-8") as data_file:
    g = json.load(data_file)
e = int(input('Узнать количество номеров, установленных абонентом - введите 1, Узнать количество номеров, установленных с нужного года - введите 2: '))


if e == 1:
    a = str(input('input name: '))
    b = str(input('input filter item: '))
    filtercount(a,b,g)

    print('Номера абонента', a, ' ', filtercount(a,b,g))
    print('Количество номеров', ' ', len(filtercount(a,b,g)))

elif e == 2:
    c = str(input('Введите год начала выборки: '))
    filteryear(c,g)

    print('Количество номеров, установленных начиная с', ' ', c, 'года: ', filteryear(c,g))
    print('Номера, установленные начиная с', ' ', c, 'года: ', len(filteryear(c,g)))

else:
    print('К вводу доступны только 1 и 2')