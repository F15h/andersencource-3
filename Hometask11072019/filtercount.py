
import datetime


def filtercount(a,b,g):
    l = []
    for value in g.values():
       if value["name"] == a:
           l.append(value[b])
    return l


def filteryear(c,g):
    l = []
    d = datetime.date.today()
    for value in g.values():
       if int(value["year"]) >= int(c) and int(value["year"])<= int(d.year):
           l.append(value["number"])
    return l