# -*- coding: utf-8 -*-
import json
dict = {1: {"name": "Иванов", "year": "1994", "number": "123456"}, 2: {"name": "Петров", "year": "2001", "number": "135478"}, 3: {"name": "Сидоров", "year": "2003", "number": "325896"}, 4: {"name": "Куликов", "year": "2004", "number": "5257445"}, 5: {"name": "Куликов", "year": "2006", "number": "8662254"}, 6: {"name": "Иванов", "year": "2002", "number": "823456"}}

with open("data_file.json", "w", encoding="utf-8") as write_file:
    json.dump(dict, write_file, indent=4, ensure_ascii=False)